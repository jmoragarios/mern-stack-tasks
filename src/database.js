const mongoose = require('mongoose');
const URI = 'mongodb://localhost/mern-tasks';


mongoose.connect(URI, {
        useNewUrlParser: true,
        useFindAndModify: false
    })
    .then(db => console.log('Db is conected'))
    .catch(err => console.log(err));

module.exports = mongoose;