import React, { Component } from 'react';

class App extends Component {
    constructor(){
        super();
        this.state = {
            title: '',
            description: '',
            id: '',
            tasks: []
        }
        this.addTask = this.addTask.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.getTasks = this.getTasks.bind(this);
        this.deleteTask = this.deleteTask.bind(this);
        this.editTask = this.editTask.bind(this);
    }
    componentDidMount(){
        this.getTasks();
    }

    handleChange(e){
        const { name, value } = e.target;
        this.setState({
            [name] : value
        });
    }

    getTasks () {
        fetch('/api/tasks')
        .then(res => res.json())
        .then(tasks => {
            this.setState({
                tasks: tasks
            })
        })
    }

    addTask(e){
        e.preventDefault();
        if(this.state.id){
            fetch(`/api/tasks/${this.state.id}`,{
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                this.setState({
                    title: '',
                    description: '',
                    id: ''
                });
                M.toast({html: 'Task Updated'});
                this.getTasks();
            })
        }else{
            fetch('/api/tasks/', {
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            }).then( res => res.json())
            .then(data => {
                M.toast({html: 'Task Saved'})
                this.setState(({
                    title : '',
                    description: ''
                }))
                this.getTasks()
            })
            .catch(err => console.log(err));
        }
    }

    deleteTask(id) {
        if (confirm('Are you sure you want to delete it?')) {
            fetch(`/api/tasks/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({ html: 'Task Deleted' });
                    this.getTasks();
                });
        }
    }

    editTask(id) {
        fetch(`/api/tasks/${id}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            this.setState({
                title : data.title,
                description : data.description,
                id : data._id
            });
            console.log(this.state.id);
            
        })
    }

    render(){
        return (
            <div>
                {/* NAVIGATION */}
                <nav className="blue darken-4">
                    <div className="container">
                        <a className="brand-logo" href="#">Tasks</a>
                    </div>
                </nav>
                <div className="container">
                    <div className="row">
                        <div className="col s5">
                            <div className="card">
                                <div className="card-content">
                                    <form onSubmit={this.addTask}>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <input name="title" onChange={this.handleChange} placeholder="Task Title" type="text" value={this.state.title}/>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="input-field col s12">
                                                <textarea className="materialize-textarea" name="description" onChange={this.handleChange} placeholder="Task Description" value={this.state.description}></textarea>
                                            </div>
                                        </div>
                                        <button type="submit" className="btn blue darken-4">Send</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col s7">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        this.state.tasks.map(task => {
                                            return(
                                                <tr key={task._id}>
                                                    <td>{task.title}</td>
                                                    <td>{task.description}</td>
                                                    <td>
                                                        <button className="btn light-blue darken-4" onClick={() => this.editTask(task._id)}><i className="material-icons">edit</i></button>
                                                        <button className="btn light-blue darken-4" style={{margin: '4px'}} onClick={() => this.deleteTask(task._id)}><i className="material-icons">delete</i></button>
                                                    </td>
                                                </tr>
                                            )
                                        })
                                        
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

export default App;