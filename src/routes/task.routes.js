const express = require('express');
const router = express.Router();
const Task = require('../models/task');

router.get('/tasks', async (req, res) => {
    var tasks = await Task.find();
    
    res.json(tasks);
});

router.get('/tasks/:id', async (req, res)=> {
    const task = await Task.findById(req.params.id);
    res.status(200);
    res.json(task);
})

router.post('/tasks', async (req, res) => {
    var newTask = new Task(req.body);
    await newTask.save();
    res.status(200);
    res.json({
        status: 'Task saved'
    });
});

router.put('/tasks/:id', async (req, res) => {
    
    await Task.findByIdAndUpdate(req.params.id, req.body);
    res.status(200);
    res.json({status: 'task updated'});
});

router.delete('/tasks/:id', async (req, res) => {

    await Task.findByIdAndDelete(req.params.id);
    res.status(200);
    res.json({status: 'task deleted'});
});

module.exports = router;