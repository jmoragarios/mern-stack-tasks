//Dependencies
const express = require('express');
const morgan = require('morgan');
const routes = require('./routes/task.routes');
const path = require('path');
const app = new express();
const { mongoose } = require('./database');

//Settings
app.set('port',process.env.PORT || 3000);
//Middlewares
app.use(morgan('dev'));
app.use(express.json());

//Routes
app.use('/api',routes);
//Static files
app.use(express.static(path.join(__dirname, 'public')));


//starting server
app.listen(app.get('port'), () => {
    console.log(`Server listen on port ${app.get('port')}`);
});