module.exports = {
    entry: [
        './src/app/index.js'
    ],
    output: {
        path: __dirname + '/src/public',
        filename: 'bundle.js'
    },
    mode : 'development',
    module: {
        rules: [
            {
                test: /\.js/,
                loader: 'babel-loader',
                exclude: /node_modules/
            }          
        ]
    },
    
}